/*****************************************************************************
 
 greyscale.cpp :
 
 Author:
 Based On: simple_bus_direct.cpp
 
 *****************************************************************************/

#include "greyscale.h"

#include "../mem/addr.h"
#include "../util/stages.h"
#include "../util/log.h"
#include "../util/img_util.h"


//#include <opencv2/imgproc.hpp>
//#include <opencv2/highgui.hpp>
//#include <opencv2/core.hpp>
//#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>

#include <cmath>

using namespace cv;
using namespace std;

void greyscale::main_action() {
	while (true) {

		// wait for my turn
		 while (stage != stages::ROTATING_IMG_DONE) {
			 wait();
			bus_port->direct_read(&stage, addr::STAGE);
		}


		// change stage
		stage = stages::CONVERTING_TO_GRAY;
		bus_port->direct_write(&stage, addr::STAGE);
		if (m_verbose) { printf("\n"); log::d(name(), "STARTING CONVERTION TO GREY\n"); }


		// get image dimension
		bus_port->direct_read(&width , addr::IMG_WIDTH);
		bus_port->direct_read(&height, addr::IMG_HEIGHT);
		blocksize = (width*height)*4;


		// get the start address of the previous stage
		int start_address;
		bus_port->direct_read(&start_address, addr::ROTATED_IMG);        


		// get all data of the previous stage
		if (m_verbose) { log::d(name(), "getting data from previous stage\n"); }
		buffer = new int[blocksize];
		bus_port->burst_read(/*unique_priority*/ 1, buffer, start_address, blocksize, /*lock*/ true);


		// main action
		execute_task();


		// change the my stage to done
		stage = stages::CONVERTING_TO_GRAY_DONE;
		bus_port->direct_write(&stage, addr::STAGE);
		if (m_verbose) { log::d(name(), "CONVERTION TO GREY DONE\n"); }

	}

}

void greyscale::execute_task() {

    //user/local/include/opencv2/core/hal/interface.h -> int64 e uint64

    Mat image(height, width, CV_8UC4);
    Mat gray_image(height, width, CV_8UC4);
 
    int aux = 0;
	for (int i = 0; i < image.rows; i++) 
    {
		for (int j = 0; j < image.cols; j++) 
        {          
            image.at<Vec4b>(i, j)[2] = buffer[aux + 0]; 
			image.at<Vec4b>(i, j)[1] = buffer[aux + 1]; 
			image.at<Vec4b>(i, j)[0] = buffer[aux + 2]; 
			image.at<Vec4b>(i, j)[3] = buffer[aux + 3]; 
			aux += 4;
		}
	}
    
    cvtColor(image.clone(), gray_image, COLOR_BGR2GRAY);
    
//    assert(img_util::test_rgb2gray_img(gray_image, buffer));

    int data[4];
    unsigned int temp = 0;
	for (int i = 0; i < blocksize; i=i+4) 
    {		

        data[0] = gray_image.data[temp];   //R
        data[1] = gray_image.data[temp];   //G
        data[2] = gray_image.data[temp];   //R
		data[3] = image.data[i+3];         //A
		
		if (m_verbose) log::d(name(), "writing pixel (%d)\n", i-4);
		if (m_verbose) log::d(name(), "writing pixel value in R (%d)\n", data[0]);
		if (m_verbose) log::d(name(), "writing pixel value in G (%d)\n", data[1]);
		if (m_verbose) log::d(name(), "writing pixel value in B (%d)\n", data[2]);
		if (m_verbose) log::d(name(), "writing pixel value in A (%d)\n", data[3]);
		bus_port->burst_write(0, data, m_address, 4, true);

        temp++;
		m_address += 16;
	}


	// write result image on disk
	cv::imwrite(img_util::get_project_dir() + "/src/_resources/generated/" + "2_grey.jpg", gray_image);



}
