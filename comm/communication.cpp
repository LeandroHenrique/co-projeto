/*****************************************************************************
 
 communication.cpp : The communication interface (master) used to receive
 packages and store them in memory using the direct BUS interface.
 
 Author: Leandro
 Based On: simple_bus_direct.cpp
 
 *****************************************************************************/

#include "communication.h"

#include "../mem/addr.h"
#include "../sb/simple_bus_types.h"
#include "../util/img_util.h"
#include "../util/stages.h"
#include "../util/package.h"
#include "../util/log.h"


void communication::main_action() {


	// load image
	Mat img = imread(img_util::get_project_dir() + "/src/_resources/original/meninas2.jpg");
	assert(!img.empty());
	log::d(name(), "Communication starting (%d x %d)\n", img.cols, img.rows);


	// write image info on memory
	stage = stages::RECEIVING_IMG;
	bus_port->direct_write(&stage   , addr::STAGE);
	bus_port->direct_write(&img.cols, addr::IMG_WIDTH);
	bus_port->direct_write(&img.rows, addr::IMG_HEIGHT);


	// send pixels
	int *payload = new int[4];
	int w = img.cols;
	int h = img.rows;

	buffer = new int[w*h*4];
	int count;

	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {

			payload[0] = img.at<Vec3b>(i, j)[2]; // R
			payload[1] = img.at<Vec3b>(i, j)[1]; // G
			payload[2] = img.at<Vec3b>(i, j)[0]; // B
			payload[3] = 255;                    // A

			buffer[count++] = payload[0];
			buffer[count++] = payload[1];
			buffer[count++] = payload[2];
			buffer[count++] = payload[3];


			bus_port->burst_write(0, payload, m_address, 4, false);
			m_address += 16;

		}
	}

	img_util::write("0_original", buffer, img.cols, img.rows);

	// communication done
	log::d(name(), "Communication done\n");
	stage = stages::RECEIVING_IMG_DONE;
	bus_port->direct_write(&stage, addr::STAGE);


}

