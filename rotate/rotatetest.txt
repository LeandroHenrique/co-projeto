//mudar extensao do arquivo para .cpp

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int main()
{
	//Mat img = imread("lena.jpeg");
	Mat img = imread("wellton.jpg");	
	//Mat img1 = img.clone();
	Mat img1 = Mat::zeros(img.rows, img.cols, CV_8UC3);
	Mat rot1 = Mat::zeros(img.cols, img.rows, CV_8UC3); // troca linha por coluna
	int aux = img.rows*img.cols*3;
	//int buffer[aux];
	int *buffer = new int[aux];
	int height = img.rows;//linhas
	int width = img.cols;//colunas
	
	int*** mat;//matriz de 3 dimensoes --> mat[width][height][4]
	int*** rot;//matriz de 3 dimensoes --> rot[height][width][4]

	mat = new int**[height];
	for (int i=0; i<height; i++) {
		mat[i] = new int*[width];
	        for (int j=0; j<width; j++) {
        		mat[i][j] = new int[3];
	       }
	}
  
	rot = new int**[width];
	for (int i=0; i<width; i++) {
		rot[i] = new int*[height];
	        for (int j=0; j<height; j++) {
        		rot[i][j] = new int[3];
	       }
	}



	for (int i = height - 1; i >=0; i--) {//joga o rot(Mat) de volta no buffer
		for (int j = width - 1; j >=0; j--) {
			buffer[aux - 1] = (unsigned int)img.at<Vec3b>(i,j)[2];//R
			buffer[aux - 2] = (unsigned int)img.at<Vec3b>(i,j)[1];//G
			buffer[aux - 3] = (unsigned int)img.at<Vec3b>(i,j)[0];//B
			aux = aux - 3;
		}
	}
	  
	aux = img.rows*img.cols*3; 

	for (int i = height - 1; i >=0; i--) {//joga o buffer em img(Mat)
		for (int j = width - 1; j >=0; j--) {
			mat[i][j][0] = buffer[aux - 1];
			mat[i][j][1] = buffer[aux - 2];
			mat[i][j][2] = buffer[aux - 3];
			aux = aux - 3;
		}
	}

	for (int i=0; i<width; i++) {//rotaciona no sentido anti-horario
            for (int j=0; j<height; j++) {
		for (int k=0; k<3; k++) {
                	rot[i][j][k] = mat[j][width - i -1][k];
		}
            }
        }

	aux = img.rows*img.cols*3;

	for (int i = width - 1; i >=0; i--) {//joga o rot(Mat) de volta no buffer
		for (int j = height - 1; j >=0; j--) {
			buffer[aux - 1] = rot[i][j][0];//R
			buffer[aux - 2] = rot[i][j][1];//G
			buffer[aux - 3] = rot[i][j][2];//B
			aux = aux - 3;
		}
	}

	aux = img.rows*img.cols*3;
	for (int i = width - 1; i >=0; i--) {//joga o buffer em img(Mat)
		for (int j = height - 1; j >=0; j--) {
			rot1.at<Vec3b>(i,j)[2] = (unsigned char)buffer[aux - 1];
			rot1.at<Vec3b>(i,j)[1] = (unsigned char)buffer[aux - 2];
			rot1.at<Vec3b>(i,j)[0] = (unsigned char)buffer[aux - 3];
			aux = aux - 3;
		}
	}
	/*for (int i = height - 1; i >=0; i--) {//joga o rot(Mat) de volta no buffer
		for (int j = width - 1; j >=0; j--) {
			buffer[aux - 1] = (unsigned int)img.at<Vec3b>(i,j)[2];//R
			buffer[aux - 2] = (unsigned int)img.at<Vec3b>(i,j)[1];//G
			buffer[aux - 3] = (unsigned int)img.at<Vec3b>(i,j)[0];//B
			aux = aux - 3;
		}
	}
	buffer[aux-1] = (unsigned int)img.at<Vec3b>(height-1,width-1)[3];
	
		//cout << "array: " << buffer[aux - 1] <<" mat: "<< (unsigned int)img.at<Vec3b>(height,width)[0] << endl;
	
	aux = img.rows*img.cols*3;

	for (int i = height - 1; i >=0; i--) {//joga o buffer em img(Mat)
		for (int j = width - 1; j >=0; j--) {
			img1.at<Vec3b>(i,j)[2] = (unsigned char)buffer[aux - 1];
			img1.at<Vec3b>(i,j)[1] = (unsigned char)buffer[aux - 2];
			img1.at<Vec3b>(i,j)[0] = (unsigned char)buffer[aux - 3];
			aux = aux - 3;
		}
	}
	cout << "array: " << (unsigned int)img.at<Vec3b>(height,width)[0] <<" array1: "<< (unsigned int)img1.at<Vec3b>(height,width)[0] << endl;
	

        for (int i=0; i<img.cols; i++) {
            for (int j=0; j<img.rows; j++) {
                rot.at<Vec3b>(i, j)[2] = img.at<Vec3b>(j, img.cols - i - 1)[2];
		rot.at<Vec3b>(i, j)[1] = img.at<Vec3b>(j, img.cols - i - 1)[1];
		rot.at<Vec3b>(i, j)[0] = img.at<Vec3b>(j, img.cols - i - 1)[0];
            }
        }*/
	
	//rotate(img, img1, ROTATE_90_COUNTERCLOCKWISE);

	namedWindow("img", WINDOW_NORMAL);
	imshow("img", img);

	namedWindow("img1", WINDOW_NORMAL);
	imshow("img1", rot1);

	/*namedWindow("r", WINDOW_NORMAL);
	imshow("r", rot);*/

	free(buffer);

	waitKey(0);

}

