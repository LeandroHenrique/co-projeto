/*****************************************************************************
 
 rotation.cpp :
 
 Author:
 Based On: simple_bus_direct.cpp
 
 *****************************************************************************/

#include "rotation.h"

#include "../mem/addr.h"
#include "../util/img_util.h"
#include "../util/stages.h"
#include "../util/log.h"

using namespace cv;

void rotation::main_action() {
	while (true) {

		// wait for my turn
		while (stage != stages::RECEIVING_IMG_DONE) {
			wait();
			bus_port->direct_read(&stage, addr::STAGE);
		}


		// change stage
		stage = stages::ROTATING_IMG;
		bus_port->direct_write(&stage, addr::STAGE);
		printf("\n"); log::d(name(), "STARTING ROTATING IMAGE\n");


		// get image dimension
		bus_port->direct_read(&width , addr::IMG_WIDTH);
		bus_port->direct_read(&height, addr::IMG_HEIGHT);
		blocksize = (width*height)*4;


		// get the start address of the previous stage
		int start_address;
		bus_port->direct_read(&start_address, addr::ORIGINAL_IMG);


		// get all data of the previous stage
		if (m_verbose) { log::d(name(), "getting data from previous stage\n"); }
		buffer  = new int[blocksize];
		rotated = new int[blocksize];
		bus_port->burst_read(/*unique_priority*/ 1, buffer, start_address, blocksize, /*lock*/ true);


		// main action
		execute_task();

		// write result image on disk
		img_util::write("1_rotated", rotated, height, width);

		// test rotation
//		img_util::test_rotate_img(buffer, rotated, height, width);


		// change for new width and height
		bus_port->direct_write(&height, addr::IMG_WIDTH);
		bus_port->direct_write(&width,  addr::IMG_HEIGHT);


		// change the my stage to done
		stage = stages::ROTATING_IMG_DONE;
		bus_port->direct_write(&stage, addr::STAGE);
		log::d(name(), "ROTATE IMG DONE\n");

	}

}

void rotation::execute_task() {


	int aux = blocksize;
	int*** mat;//matriz de 3 dimensoes --> mat[width][height][4]
	int*** rot;//matriz de 3 dimensoes --> rot[height][width][4]
	
	//CRIAÇAO DAS MATRIZES TRIDIMENSIONAIS --> mat = IMAGEM NORMAL ; rot = IMAGEM ROTACIONADA
	mat = new int**[height];
	for (int i=0; i<height; i++) {
		mat[i] = new int*[width];
	        for (int j=0; j<width; j++) {
        		mat[i][j] = new int[4];
	       }
	}
  
	rot = new int**[width];
	for (int i=0; i<width; i++) {
		rot[i] = new int*[height];
	        for (int j=0; j<height; j++) {
        		rot[i][j] = new int[4];
	       }
	}
	
	//CARREGA O BUFFER NA MATRIZ 3D
	for (int i = height - 1; i >=0; i--) {//joga o buffer em img(Mat)
		for (int j = width - 1; j >=0; j--) {
			mat[i][j][0] = buffer[aux - 1];//R
			mat[i][j][1] = buffer[aux - 2];//G
			mat[i][j][2] = buffer[aux - 3];//B
			mat[i][j][3] = buffer[aux - 4];//A
			aux = aux - 4;
		}
	}

	//ROTACIONA A MATRIZ NO SENTIDO ANTI-HORARIO
	for (int i=0; i<width; i++) {
		for (int j=0; j<height; j++) {
			for (int k=0; k<4; k++) {
				rot[i][j][k] = mat[j][width - i -1][k];
			}
		}
	}
	

	//ESCREVE MATRIZ ROTACIONADA EM "ROTATED"
	aux = blocksize;

	for (int i = width - 1; i >=0; i--) {
		for (int j = height - 1; j >=0; j--) {
			rotated[aux - 1] = rot[i][j][0];//R
			rotated[aux - 2] = rot[i][j][1];//G
			rotated[aux - 3] = rot[i][j][2];//B
			rotated[aux - 4] = rot[i][j][3];//A
			aux = aux - 4;
		}
	}


	int data[4];

	for (int i = 0; i < blocksize; i = i + 4) {

		data[0] = rotated[i+0];
		data[1] = rotated[i+1];
		data[2] = rotated[i+2];
		data[3] = rotated[i+3];

		bus_port->burst_write(0, data, m_address, 4, true);

		if (m_verbose) log::d(name(), "writing pixel value in R (%d)\n", data[0]);
		if (m_verbose) log::d(name(), "writing pixel value in G (%d)\n", data[1]);
		if (m_verbose) log::d(name(), "writing pixel value in B (%d)\n", data[2]);
		if (m_verbose) log::d(name(), "writing pixel value in A (%d)\n", data[3]);

		m_address += 16;
		wait();

	}


}

