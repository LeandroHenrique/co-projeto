/*
 * log.h: log that shows:
 * - modulo name
 * - timestamp
 * - message
 *
 * Author: leandro
 *
 */

#ifndef LOG_H_
#define LOG_H_

#include <systemc>

#include <stdio.h>
#include <stdarg.h>
#include <errno.h>

class log {
public:
	virtual ~log();
	static void d(const char* name, const char* msg, ...);
	static void e(const char* name, const char* msg, ...);
};


/** log for debug */
inline void log::d(const char* name, const char* msg, ...) {

	printf("%-7s | %-12s | ", sc_time_stamp().to_string().c_str(), name);

	va_list ap;
	va_start(ap, msg);

	do {
		errno = 0;
		vfprintf(stdout, msg, ap);
	} while (errno == EINTR);

}


/** log for error */
inline void log::e(const char* name, const char* msg, ...) {

	fprintf(stderr, "%-7s | %-12s | ", sc_time_stamp().to_string().c_str(), name);

	va_list ap;
	va_start(ap, msg);

	do {
		errno = 0;
		vfprintf(stderr, msg, ap);
	} while (errno == EINTR);

}


#endif
