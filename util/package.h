/*
 * 	package.h: a package that contains: crc, type and payload[r, g, b, a]
 *
 *  Author: Leandro
 */

#ifndef package_h
#define package_h


class package {

public:

	// package types
	int static const PKG_INITIAL = 0;
	int static const PKG_DATA    = 1;
	int static const PKG_DONE    = 2;

	// constructor
	package(int crc, int type, int pl[4]) : crc(crc), type(type) {
		for (int i = 0; i < 4; i++) {
			payload[i] = pl[i];
		}
	}

	virtual ~package();

	virtual int  getCRC();
	virtual int  getType();
	virtual int* getPayLoad();

	virtual int*  getR();
	virtual int*  getG();
	virtual int*  getB();
	virtual int*  getA();

	virtual int*  getImgWidth();
	virtual int*  getImgHeight();

	virtual bool isInitialPkg();
	virtual bool isDataPkg();
	virtual bool isDonePkg();

	virtual bool ckeckCRC();

private:
	int crc;
	int type;
	int payload[4];
};


inline int  package::getCRC()        { return crc;     }
inline int  package::getType()       { return type;    }
inline int* package::getPayLoad()    { return payload; }

inline int*  package::getR()         { return &payload[0]; }
inline int*  package::getG()         { return &payload[1]; }
inline int*  package::getB()         { return &payload[2]; }
inline int*  package::getA()         { return &payload[3]; }

inline int* package::getImgWidth()   { assert(type == PKG_INITIAL); return &payload[0]; }
inline int* package::getImgHeight()  { assert(type == PKG_INITIAL); return &payload[1]; }

inline bool package::isInitialPkg()  { return type == PKG_INITIAL; }
inline bool package::isDataPkg()     { return type == PKG_DATA;    }
inline bool package::isDonePkg()     { return type == PKG_DONE;    }


inline bool package::ckeckCRC()      { return crc == (payload[0] + payload[1] + payload[2] + payload[3]); }


inline package::~package() { /* nothing */ }


#endif /* __PACKAGE_H_ */
