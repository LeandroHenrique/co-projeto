/*
 * img_util.h: utilities functions to deal with images
 *
 */

#ifndef IMG_UTIL_H_
#define IMG_UTIL_H_

#include <opencv2/opencv.hpp>

#include <unistd.h>
#define GetCurrentDir getcwd

using namespace cv;
using namespace std;


class img_util {


public:
	virtual ~img_util();
	static void write(string, int* data, int width, int height);
	static void test_rotate_img(int *original, int *rotated, int width, int height);
	static bool test_rgb2gray_img(Mat gray_image_cv, int* img);
	static string get_project_dir();
};



/** write image on "/_resources/generated" */
inline void img_util::write(string filename, int* data, int width, int height) {

	Mat img = Mat(height, width, CV_8UC4, data);
	int aux;

	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {

			img.at<Vec4b>(i, j)[2] = data[aux + 0]; // R
			img.at<Vec4b>(i, j)[1] = data[aux + 1]; // G
			img.at<Vec4b>(i, j)[0] = data[aux + 2]; // B
			img.at<Vec4b>(i, j)[3] = data[aux + 3]; // A

			aux += 4;

		}
	}

	imwrite(img_util::get_project_dir() + "/src/_resources/generated/" + filename + ".jpg", img);


}

/** compare pixel by pixel my image rotated with the image rotated by opencv algorithm */
inline void img_util::test_rotate_img(int *original, int *rotated, int width, int height) {


	/************************************************************/
	/*********** convert original image array to mat ************/
	/************************************************************/
	Mat img = Mat(width, height, CV_8UC4, original);
	int aux;

	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {

			img.at<Vec4b>(i, j)[2] = original[aux + 0]; // R
			img.at<Vec4b>(i, j)[1] = original[aux + 1]; // G
			img.at<Vec4b>(i, j)[0] = original[aux + 2]; // B
			img.at<Vec4b>(i, j)[3] = original[aux + 3]; // A

			aux += 4;

		}
	}


	/************************************************************/
	/******************* rotate using opencv ********************/
	/************************************************************/
	Mat dst;
	cv::transpose(img, dst);
	cv::flip(dst, dst, 0);

	imwrite(img_util::get_project_dir() + "/src/_resources/generated/" + "1_rotated_cv" + ".jpg", dst);


	/************************************************************/
	/****************** compare pixel by pixel ******************/
	/************************************************************/
	aux = 0;
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {

			assert(dst.at<Vec4b>(i, j)[2] == rotated[aux+0]); // R
			assert(dst.at<Vec4b>(i, j)[1] == rotated[aux+1]); // G
			assert(dst.at<Vec4b>(i, j)[0] == rotated[aux+2]); // B
			assert(dst.at<Vec4b>(i, j)[3] == rotated[aux+3]); // A

			aux += 4;

		}
	}

}

inline bool img_util::test_rgb2gray_img(Mat gray_image_cv, int* img)
{
    int* data = new int[gray_image_cv.total()*4];
	int value;      
    for(unsigned int i = 0; i < gray_image_cv.total()*4; i=i+4)
    {  
        value = round(img[i]*0.299f + img[i+1]*0.587f + img[i+2]*0.114f);

        data[i]   = value;
        data[i+1] = value;
        data[i+2] = value;
		data[i+3] = img[i+3];
    }

    unsigned int temp = 0;    
    for(unsigned int i = 0; i < gray_image_cv.total()*4; i=i+4)
    {        
        for(unsigned int j = i; j < i+3; j++)
        {
            if((unsigned int)gray_image_cv.data[temp] != (unsigned int)data[j])
            {
                // cout << "g[" << temp_ << "]: " << (unsigned int)gray_image_cv.data[temp_] << "; d[" << j <<"]: " << (unsigned int)data[j] << endl; 
                return false;
            }
        }
        temp++;        
    }


    img_util::write("2_grey_cv", data, gray_image_cv.cols, gray_image_cv.rows);
    return true;
}

/** get project root directory */
inline string img_util::get_project_dir( void ) {
	char buff[FILENAME_MAX];
	GetCurrentDir( buff, FILENAME_MAX );
	string current_working_dir(buff);
	return current_working_dir;
}



#endif
