/*****************************************************************************
 
 encryption.cpp :
 
 Author:
 Based On: simple_bus_direct.cpp
 
 *****************************************************************************/

#include "encryption.h"

#include "../mem/addr.h"
#include "../util/img_util.h"
#include "../util/stages.h"
#include "../util/log.h"


void encryption::main_action() {
	while (true) {

		// wait for my turn
		while (stage != stages::CONVERTING_TO_GRAY_DONE) {
			wait();
			bus_port->direct_read(&stage, addr::STAGE);
		}


		// change stage
		stage = stages::ENCRYPTING_IMG;
		bus_port->direct_write(&stage, addr::STAGE);
		if (m_verbose) { printf("\n"); log::d(name(), "STARTING ENCRYPTING\n"); }


		// get image dimension
		bus_port->direct_read(&width , addr::IMG_WIDTH);
		bus_port->direct_read(&height, addr::IMG_HEIGHT);
		blocksize = (width*height)*4;


		// get the start address of the original image
		int start_address;
		bus_port->direct_read(&start_address, addr::GREY_IMG);


		// get all data of the original image
		if (m_verbose) { log::d(name(), "getting data from previous stage\n"); }
		buffer = new int[blocksize];
		bus_port->burst_read(/*unique_priority*/ 1, buffer, start_address, blocksize, /*lock*/ true);


		// main action
		execute_task();


		// write result image on disk
		img_util::write("3_encrypted", buffer, width, height);


		// change the my stage to done
		stage = stages::ENCRYPTING_IMG_DONE;
		bus_port->direct_write(&stage, addr::STAGE);
		if (m_verbose) { log::d(name(), "ENCRYPTION DONE\n"); }


		// end application
		sc_stop();

	}

}

int encryption::encrypt (int a) {

    int bas1,bas2,bas3;
	int aux;

	if ( (0 < a)&& (a< 255) ) {
          bas1= a/100;
          bas2= a%10;
          bas3= (a%100 -bas2)/10;
          aux = ((bas1+7)%10)*100 
		+ ((bas2+7)%10)*10 
		+ ((bas3+7)%10);
	}

	return aux;
}

void encryption::execute_task() {

	log::d(name(), "Encry iniciando\n");

	int data[4];
	int value;

	for (int i = 0; i < blocksize; i=i+4) {
		value = encrypt(buffer[i]);

		for(int j = 0; j < 3; j++)
				data[j] = value;


		data[3] = buffer[i+3];


		log::d(name(), "writing pixel (%d)\n", i);
		log::d(name(), "Valor do buffer a ser criptografado (%d)\n", buffer[i]);
		log::d(name(), "Encrypt (%d)\n", value);
		bus_port->burst_write(0, data, m_address, 4, true);
		m_address += 16;


		// code added by leandro:
		buffer[i    ] = value;
		buffer[i + 1] = value;
		buffer[i + 2] = value;



	}
	
	log::d(name(), "Encry done\n");
}


